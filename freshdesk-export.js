#!/usr/bin/env node

// freshdesk-export.js
//------------------------------
//
// 2012-11-15, Jonas Colmsjö
//
// Copyright Gizur AB 2012
//
// Export the contacts in a freshdesk installation to a CSV file
//
// dependencies: npm install jsdom xmlhttprequest jQuery optimist
//
// Using Google JavaScript Style Guide - http://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml
//
//------------------------------



(function(){

"use strict"

// Includes
// ================

var $       = require('jQuery');
var helpers = require('./helpers.js');
var argv    = require('optimist')
                .usage('Usage: ./freshdesk-export --user [freshdesk user] --password [freshdesk password] --module [contacts]')
                .demand(['user', 'password', 'module'])
                .argv;


//
// List public gists for a user
//-----------------------------

function listContacts(user, password) {

    logDebug('listContacts: Starting getting freshdesk contacts for gizur...');

    var request = $.ajax({

        // REST function to use
        url: 'http://gizur.freshdesk.com/contacts.xml',
        type: 'GET',

        dataType: 'xml',

        success: function(data){
            logDebug('listContacts: Yea, it worked...' + data );
        },

        error: function(data){
            logErr('listContacts: Shit hit the fan...' + data );

        }

    });

    return request;

}


// Main
//=========

listContacts(argv.user, argv.password);


})();
