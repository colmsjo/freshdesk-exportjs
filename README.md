# freshdesk-exportjs

Export Freshdesk data

## Getting Started
Install the module with: `npm install freshdesk-exportjs`

```javascript
var freshdesk_exportjs = require('freshdesk-exportjs');
freshdesk_exportjs.awesome(); // "awesome"
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [grunt](https://github.com/gruntjs/grunt).

## Release History
_(Nothing yet)_

## License
Copyright (c) 2012 Jonas Colmsjö  
Licensed under the MIT license.
